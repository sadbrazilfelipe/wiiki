const db = new Irmin('http://localhost:8080/graphql')
const auth = new Irmin('http://localhost:8081/')
let toggledAttributes = [];

const _isEqual = (a, b) => {
    let i = a.length;
    while (i--) {
        if (a[i] !== b[i]) return false;
    }
    return true
}

const path = window.location.pathname
    .split('/')
    .filter(x => x !== '')

const origin = path[0] ?? 'epoch'
const title = path[1] ?? 'WelcomeVisitors'

const reset = () => {
    ['article', 'fork', 'edit', 'insert'].forEach(id => {
        const e = document.getElementById(id)
        e.setAttribute(
            'style',
            id === 'article'
            ? 'display: block;'
            : 'display: none;'
        )
    })
    toggledAttributes = []
}

const toggle = (ids) => {
    // Nothing is open. Enable.
    if (!toggledAttributes.length) {
        ids.forEach(id => {
            const e = document.getElementById(id)
            e.setAttribute(
                'style'
            ,   window.getComputedStyle(e) .display === 'block'
                ? 'display: none' : 'display: block')
            })
        toggledAttributes = ids
    }
    // Same form was open. Disable.
    else if (_isEqual(toggledAttributes, ids)) {
        reset()
    }
    // A different form was open. Disable that and try again.
    else {
        reset()
        toggle(ids)
    }
}

const _fork = (secret, namespace) => {
    fetch('/fork', {
        method: 'POST',
        body: JSON.stringify({
            to: namespace,
            secret: secret,
            title: title,
            from: origin
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
    })
}

const _set = (secret, content) => {
    fetch('/set', {
        method: 'POST',
        body: JSON.stringify({
            to: origin,
            title: title,
            content: content,
            from: origin,
            secret: secret
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
    })
}

const _new = (secret, content) => {
    fetch('/new', {
        method: 'POST',
        body: JSON.stringify({
            to: origin,
            title: title,
            content: content,
            secret: secret
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        }
    })
}

window.onload = () => {    

    document.getElementById('edit').addEventListener('submit', e => {
        e.preventDefault()
        _set(edit.secret.value, edit.content.value)
    })

    document.getElementById('fork').addEventListener('submit', e => {
        e.preventDefault()
        _fork(fork.secret.value, fork.namespace.value)
    })
    
    document.getElementById('insert').addEventListener('submit', e => {
        e.preventDefault()
        _new(insert.namespace.value, insert.title.value, insert.secret.value)
    })
}
