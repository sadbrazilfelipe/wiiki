const transform = require('./transform')
const fs = require('fs')
const path = require('path')
const util = require('./util.js')

async function main() {
    let { todo, internalLinks } = readPriorState()
    let finished = []

    for (let i = 0, len = todo.length; i < len; i++) {
        let article = todo[i]
        
        
        // Write increments to save progress
        if (util.multipleOf(500, i)) {
            util.writeBatch('../.compiled/articles.json', finished)
            finished = []
        }
        
        // If the article is broken its text property will be missing
        // Currently it not possible to retrieve these, so they are ignored
        if (article.text) {
            article.text = await transform(article.text, internalLinks)
            finished.push(article)
        }
    }
    // Write remainder
    writeBatch('../.compiled/articles.json', finished)
}


/**
 * Determines the work done thus far and proceeds from there.
 * @returns {Array} Object.todo The remaining articles
 * @returns {Set} Object.internalLinks A lists of article titles that should be considered internal
 */
function readPriorState() {
    let all = fetchAllTitles()
    let alreadyDone = fetchAlreadyDone()
    let work = fetchArticles()
    
    let todoTitles = util.difference(new Set(all),
                                     new Set(alreadyDone.map(x => x.title)))
    
    return {
        todo: work.filter(x => todoTitles.has(x.title)),
        internalLinks: todoTitles
    }

    function fetchAllTitles() {
        let json = fs.readFileSync(path.join(__dirname, '../c2/names.json'))
        return JSON.parse(json)
    }

    function fetchArticles() {
        let json = fs.readFileSync(path.join(__dirname, '../c2/articles_c2.json'))
        return JSON.parse(json)
    }

    function fetchAlreadyDone() {
        let alreadyDoneUnclosedJson = fs.readFileSync(path.join(__dirname, '../.compiled/articles.json'))
        let closed = util.closeJsonArray(alreadyDoneUnclosedJson)
        let alreadyDone = JSON.parse(closed)

        return alreadyDone
    }
}

main()