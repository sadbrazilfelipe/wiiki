
const fs = require('fs')
const path = require('path')

/**
 * This is a hack to append objects to a JSON without actually parsing it.
 * @param {Array} x Some unclosed JSON
 * @returns Closed, valid JSON
 */
module.exports.closeJsonArray = function (x) {
    return x.slice(0, -2) + ']'
}

module.exports.multipleOf = function (m, x) {
    return x % m == 0 && m != 0
}

/**
 * Difference of two sets
 * @param {Set} A 
 * @param {Set} B 
 * @returns {Set} A - B
 */
module.exports.difference = (A, B) => new Set([...A].filter(x => !B.has(x)))

/**
 * 
 * @param {string} relativePath
 * @param {Array} batch   
 */
 module.exports.writeBatch = function (relativePath, batch) {
    let lines = batch.map(x => JSON.stringify(x)).join(',\n')
    fs.appendFile(relativePath, lines, (err) => {if (err) {console.error(err)}})
}