const wikiparser = require('./wikiparser')
const execa = require('execa')
const stream = require('stream')


/**
 * @param {string} text The text to transform
 * @param {Map} articleTitles A list of titles to flag as internal links
 * @return {string} The transformed text 
 */
module.exports = async (text, articleTitles) => {
    let badHtml = await wikiparser(text, articleTitles)
    let markdown = await pandoc(badHtml)
    let normalized = normalize(markdown)
    
    return normalized
}


// Normalize to 4 spaces for compatibility and also fix blockquotes
function normalize(markdown) {
    return markdown
        .replace(/^: {7}/gm, ':   ')  // Reduce deflists to 3 spaces from 7
        .replace(/^>$(?!\n>)/gm, '')  // Empty lines cannot be quoted unless it is part of quote

            // Todo: I have made the regexes as specific as I can but it is possible that,
            // Since we don't know our context, both rules can activate, if e.g.,
            // there exist code which is formatted like our lists
            .replace(/^ {8}(?!( *)?(- {7}|\d\. {2}))/gm, '    ')  // Reduce code block to 4 spaces
            .replace(/^( *)(?:(-) {7}|(\d\.) {2})(?=[^ ])/gm, normalizeLists)  // Reduce lists to 3 spaces
        
        // Unescape special characters for better readablity
        // We preserve &qt; and &lt; because these may be intepreted as a tag otherwise
        .replace(/;AMP;/g,  '&')
        .replace(/;QUOT;/g, '"')
}

// ToDo: Consider the odds of modifying a preformatted block (or the opposite)
/**
 * 
 * @param {string} match Unused 
 * @param {string} oldPrefix The leading indentation
 * @param {string} bullet The type of list (*, -)
 * @param {string} numbered Or potentially a numbered list
 */
function normalizeLists(match, oldPrefix, bullet, numbered) {
    let indentationLevel = oldPrefix.length / 8
    let newPrefix = ' '.repeat(indentationLevel * 4)
    let newSuffix = '  '
    let middle = bullet || numbered
    return `${newPrefix}${middle}${newSuffix}`
}

/**
 * 
 * @param {string} text Some text to pipe into pandoc
 * using no wrap to preserve line-length,
 * and tab-stops of 8, as this was originally used.
 */
async function pandoc(text) {
    const readable = stream.Readable.from(text)
    const args = ['--to=markdown', '--from=html', '--tab-stop=8', '--wrap=none']
    const res = await execa('pandoc', args, {input: readable})    
    return res.stdout
}
