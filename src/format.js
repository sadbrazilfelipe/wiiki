
const fs = require('fs')
const path = require('path')
const util = require('./util')
const synthesize = require('./synthesize')


function main() {
    let articles = readArticles()
    let synthesizedArticles = articles.map(synthesize)
    writeArticles(synthesizedArticles)
}


function writeArticles(synthesizedArticles) {
    fs.writeFileSync(path.join(__dirname, '../.compiled/synthesized.json'), JSON.stringify(synthesizedArticles))
}


function readArticles() {
    let articlesJsonUnclosed = fs.readFileSync(path.join(__dirname, '../.compiled/articles.json'))
    let closed = util.closeJsonArray(articlesJsonUnclosed)
    let articles = JSON.parse(closed)
    return articles
}

main()
