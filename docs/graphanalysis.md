This file contains some musings on what I may choose to do with the graph.

---------

* What are the most significant nodes in each community?
-- From https://community.neo4j.com/t/finding-influencers-and-communities-in-the-graph-community/7405
// Find all articles and order them by community and PageRank scores
MATCH (n)
WITH n
ORDER BY n.communityID, n.pageRank DESC

// Return top 10 ranked nodes grouped by community
WITH n.communityID AS community, collect(n.title) AS articles
RETURN community, articles[..10]
ORDER BY size(articles) DESC

---------------


* What community is largest?
  * What is the most influential node in the largest community?


* What are the most influential nodes?
MATCH (n)
RETURN n.title, n.pageRank
ORDER BY n.pageRank
DESC LIMIT 50

* What community have the highest collective influence?


* What is the most influential page in ExtremeProgramming?
MATCH (n {communityID: 359})
RETURN n.title, n.pageRank
ORDER BY n.pageRank DESC

* What are the WalledGardens? Do they form any significant networks?
  * Not really. There are 379 disconnected subgraphs, none with a greater size than 3 
  * A proper analysis would require testing the connectedness of subgraphs compared to the remainder of the graph
CALL gds.wcc.stream(
  {
    nodeProjection: 'Article',
    relationshipProjection: 'LINKS'
  }
)
YIELD nodeId, componentId
RETURN componentId, size(collect(gds.util.asNode(nodeId).title)) AS articles
ORDER BY articles DESC;

* Are there any singular dangling articles?

* Is WikiWikiWeb a small world network? What is the average distance between any two articles?