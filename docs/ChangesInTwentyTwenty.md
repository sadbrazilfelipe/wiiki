# Preamble

> How about everyone gets over it, learn to use wiki as it is, and quit trying to fix and improve everything. It's worked fine for many years and doesn't need fixing. Do you know how tiring it is watching every person come in here thinking they can fix it, or improve it, well guess what, you can't, if you could, this would be your wiki.

> In response to the above paragraph, I must concur that we should stop all forms of innovation and advancement. They are utterly ridiculous and who are we to think we can make the world a better place? Sarcasm. It is this type of inventiveness that created WikiWiki in the first place. Who is WardCunningham to think he can make something easier to collaborate with than coding with plain HTML and uploading our changes to free-for-all FTP server. :-)

-- Wiki on [SixSingleQuotes](SixSingleQuotes)

I made [post](https://www.reddit.com/r/programming/comments/76lacl/what_happened_to_the_remodeling_of_wikiwikiweb/) once asking about the remodeling of WikiWikiWeb, mispleased with progress, I gave it a shot myself.

Hopefully converting to markdown will make progress towards [visualizing the wiki](VisualizeTheWiki) once and for all, and possibly get an aggregate on participation trends based on authors in the various topics and wikipages. That would be neat I think.



# The Process

The conversion process looks like this `wikimarkup > wikiscript html* > pandoc* > markdown`

Where:
* 'wikimarkup' is the the markup found in http://c2.com/wiki/remodel/pages
* 'wikiscript html' is the html rendered by the front-end JS on http://c2.com/wiki
* 'pandoc' is the document converter
* The (*) implies content was modified prior to further processing

Modifications were done using replacements and regexes, usually passing it transforming function.

It could be an idea to write a parser for the wiki markup (which I initially tried), I do feel this is a practical idea given that I am only transforming the former output and exploiting the parser. Writing my own grammar and specification could result in a different set of unexpected features, such as the six single quotes bug---this is hard to validate across 36 000 pages.

It likely, and I suspect as much, that I have missed a great deal. Still I think this approach wins in comparison because side-effects are more managable when they are introduced on a one-by-one basis. Once erronous output is discovered, it has so far been easy to remove them.

I do welcome any constructive suggestions on this however, possibly a hybrid approach is reasonable at the 8-space to 4-space indentation step given the regular nature of regexes. What I mean by this is that the transformation assumes that lists cannot occur inside code blocks, and a guarantee of behaviour is impossible without the context, which regexes are unable to get.

## changelog.md

* Instead of [SimulateQuoteBlocks](SimulateQuoteBlocks) we now real `<blockquote>`.
* Extraneous leading whitespace in codeblocks is removed and the 8 space indentation (possibly including tabs) is now 4 space indentation and tabs are converted to spaces. Alignment is also preserved!
* The terminal sequence [SixSingleQuotes](SixSingleQuotes) has been removed.
* Formatting in code blocks has been disallowed.
* The wikiscript would output sequences such as `<strong><em>(.*)<\/strong><\/em>(.*)` and this is now likely fixed.
* A specific formatting sequence have been modified because markdown considers the characters ```\`*_{}[]()>#+-.!``` to terminate the formatting sequence, therefore leaving dangling asterisks. I don't remember if this came with any downsides, although I doubt it.
* I also redid the Logo (partially in Microsoft Paint).

# Suggested Pages
Lastly, for anyone who got this far I want to recommend some pages:

* [RoadMaps](https://wiki.kluv.in/RoadMaps) * Instructions on browsing various topics
* [MoreAboutTheLogo](https://wiki.kluv.in/MoreAboutTheLogo) * About the logo
* [VisualizeTheWiki](https://wiki.kluv.in/VisualizeTheWiki) * Visualizing the wiki
* [VisualTour](https://wiki.kluv.in/VisualTour) * More visualizing the wiki
* [ChangesInMonth](https://wiki.kluv.in/ChangesInMonth) * Saved montly recent changes logs
* [ReallyValuablePages](https://wiki.kluv.in/ReallyValuablePages) * Popular Pages
* [PatternIndex](https://wiki.kluv.in/PatternIndex) * Popular Patterns
* [CategoryCategory](https://wiki.kluv.in/CategoryCategory) * All Categories
* [HowToSumFromOneToTenInLispOrScheme](https://wiki.kluv.in/HowToSumFromOneToTenInLispOrScheme)
* [InformalHistoryOfProgrammingIdeas](https://wiki.kluv.in/InformalHistoryOfProgrammingIdeas)

## New Users and Wiki Etiquette
* [WelcomeToWikiPleaseBePolite](https://wiki.kluv.in/WelcomeToWikiPleaseBePolite)
* [WikiIsNotWikipedia](https://wiki.kluv.in/WikiIsNotWikipedia)
* [GoodStyle](https://wiki.kluv.in/GoodStyle)
* [TextFormattingRules](https://wiki.kluv.in/TextFormattingRules)
* [OneMinuteWiki](https://wiki.kluv.in/OneMinuteWiki)

## Interesting Pages
* [CategoryWikiHistory](https://wiki.kluv.in/CategoryWikiHistory)
* [WikiWikiSuggestions](https://wiki.kluv.in/WikiWikiSuggestions) * Popular Suggestions
* [WikiWikiBugs](https://wiki.kluv.in/WikiWikiBugs) * Popular Bugs
* [WikiHistory](https://wiki.kluv.in/WikiHistory) * History of the wiki
* [WalledGarden](https://wiki.kluv.in/WalledGarden) * Seperated clusters of pages
* [WalledGardens](https://wiki.kluv.in/WalledGardens) * Another page on separated clusters of pages
