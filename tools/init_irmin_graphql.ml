open Lwt.Infix

module Article = struct

  type t = {
    title : string;
    text : string;
    date : string;
  } [@@deriving irmin]

  let merge = Irmin.Merge.(option (idempotent t))
end

module Remote = struct
  let remote = None
end

module Store = Irmin_unix.Git.FS.KV (Article)
module Server = Irmin_unix.Graphql.Server.Make (Store) (Remote)


let schema_typ : (unit, Article.t option) Irmin_graphql.Server.Schema.typ =
  let open Article in
  Irmin_graphql.Server.Schema.(obj "Article"
    ~fields:(fun _ -> [
      field "title"
        ~typ:(non_null string)
        ~args:[]
        ~resolve:(fun _ t -> t.title)
      ;
      field "text"
        ~typ:(non_null string)
        ~args:[]
        ~resolve:(fun _ t -> t.text)
      ;
      field "date"
        ~typ:(non_null string)
        ~args:[]
        ~resolve:(fun _ t -> t.date)
      ;
    ])
  )

module Custom_types = struct
  module Defaults = Irmin_graphql.Server.Default_types (Store)

  (* Use the default types for most things *)
  module Key = Defaults.Key
  module Metadata = Defaults.Metadata
  module Hash = Defaults.Hash
  module Branch = Defaults.Branch

  module Contents = struct
    include Defaults.Contents
    let schema_type = schema_typ
  end
end



let main () =
  (* Set up the Irmin store *)
  Store.Repo.v (Irmin_git.config "./") >>= fun repo ->

  (* Initialize the GraphQL server *)
  let server = Server.v repo in

  (* Run the server *)
  let on_exn exn =
    Logs.debug (fun l -> l "on_exn: %s" (Printexc.to_string exn))
  in
  Cohttp_lwt_unix.Server.create ~on_exn ~mode:(`TCP (`Port 8080)) server

(* let main () =
  let config = Irmin_git.config "./" in
  Store.Repo.v config >>= fun repo ->
  let server = Server.v repo in
  let src = "localhost" in
  let port = 8080 in
  Conduit_lwt_unix.init ~src () >>= fun ctx ->
  let ctx = Cohttp_lwt_unix.Net.init ~ctx () in
  let on_exn exn = Printf.printf "on_exn: %s" (Printexc.to_string exn) in
  Printf.printf "Visit GraphiQL @ http://%s:%d/graphql\n%!" src port;
  Cohttp_lwt_unix.Server.create ~on_exn ~ctx ~mode:(`TCP (`Port port)) server *)

let () = Lwt_main.run (main ())
