open Lwt.Infix
open Core


module Store = Irmin_unix.Git.FS.KV(Irmin.Contents.Json_value)
module Tree = Store.Tree
let ( let* ) x f = Lwt.bind x f
let ( let+ ) x f = Lwt.map f x


let info = Irmin_unix.info

let storecontents_of_yojson (jsons : Yojson.Basic.t) : (Store.key * Store.contents) list =
  (* print_endline @@ Yojson.show jsons; *)
  let open Yojson.Basic.Util in
  convert_each (fun json -> 
    let title = json |> member "title" |> to_string in
    let text  = json |> member "text"  |> to_string in
    let date  = json |> member "date"  |> to_string in 
    [ title ] , `O ["title", `String title; "text", `String text; "date", `String date ])
  jsons

let make_dummy k =
  [ k ] , `O ["title", `String k; "text", `String k; "date", `String k ]

let tree_of_records records =
  Lwt_list.fold_left_s
    (fun acc (k , c) -> Tree.add acc k c)
    (Tree.empty) (records) >>= Lwt.return

 
let main () =
  let config = Irmin_git.config ~bare:true "./" in
  let* repo = Store.Repo.v config in
  let* t = Store.of_branch repo "epoch" in

  let records =
    Yojson.Basic.from_file "../.compiled/articles.json"
    |> storecontents_of_yojson in
    (* List.init 100000 ~f:succ |> List.map ~f:(fun x -> make_dummy @@ string_of_int x) in *)
    let* tree = tree_of_records records in
    print_endline "Completing";
    Store.set_tree_exn t ~info:(info "WikiWikiWeb") [ "w" ] tree
let () = Lwt_main.run (main ()); print_endline "Completed"